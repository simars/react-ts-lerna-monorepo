# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.7](https://github.com/ElementAI/@eai-material-ui/compare/@eai-material-ui/docs@0.2.6...@eai-material-ui/docs@0.2.7) (2019-05-06)

**Note:** Version bump only for package @eai-material-ui/docs





## [0.2.6](https://github.com/ElementAI/@eai-material-ui/compare/@eai-material-ui/docs@0.2.5...@eai-material-ui/docs@0.2.6) (2019-04-18)


### Bug Fixes

* **BasicIcons:** added displayname ([bfd9f46](https://github.com/ElementAI/@eai-material-ui/commit/bfd9f46))
* **build:** fixed build error around displayName ([260453b](https://github.com/ElementAI/@eai-material-ui/commit/260453b))
* **cleanup:** removed console.log ([74f2a7c](https://github.com/ElementAI/@eai-material-ui/commit/74f2a7c))
* **Fixed lint errors:** lint errors resolved ([c2d5ee4](https://github.com/ElementAI/@eai-material-ui/commit/c2d5ee4))
* **Fixed root import:** added possibility to roiot import components ([6ca0eed](https://github.com/ElementAI/@eai-material-ui/commit/6ca0eed))
* **lint:** fiexd lint errors ([1844fe3](https://github.com/ElementAI/@eai-material-ui/commit/1844fe3))
* **lint:** fixed linting errors ([d43fce9](https://github.com/ElementAI/@eai-material-ui/commit/d43fce9))
* **lint:** fixed linting errors ([5afbaca](https://github.com/ElementAI/@eai-material-ui/commit/5afbaca))
* **theme:** removed comments ([bbb1467](https://github.com/ElementAI/@eai-material-ui/commit/bbb1467))


### Features

* **Created Icons packages:** added Full Basic Icons suite ([e34af7e](https://github.com/ElementAI/@eai-material-ui/commit/e34af7e))
* **darkTheme and colors:** adjusted colors and fixed dark theme to match comments ([9869628](https://github.com/ElementAI/@eai-material-ui/commit/9869628))
* **Figured out how to iterate over * as default export:** simplefied story oficons ([75d2653](https://github.com/ElementAI/@eai-material-ui/commit/75d2653))
* **icons:** fixed icons and typographie ([6ded25b](https://github.com/ElementAI/@eai-material-ui/commit/6ded25b))
* **theme:** added dark theme and demo ([a9a3410](https://github.com/ElementAI/@eai-material-ui/commit/a9a3410))





## [0.2.5](https://github.com/ElementAI/@eai-material-ui/compare/@eai-material-ui/docs@0.2.4...@eai-material-ui/docs@0.2.5) (2019-04-01)


### Bug Fixes

* **added typography to theme:** added h1-h4 specs ([e9548a0](https://github.com/ElementAI/@eai-material-ui/commit/e9548a0))
* **theme.tsx:** added some overrides to theme ([66bc306](https://github.com/ElementAI/@eai-material-ui/commit/66bc306))





## [0.2.4](https://github.com/ElementAI/@eai-material-ui/compare/@eai-material-ui/docs@0.2.3...@eai-material-ui/docs@0.2.4) (2019-03-29)


### Bug Fixes

* **font.d.ts:** finally got cmd working ([1927115](https://github.com/ElementAI/@eai-material-ui/commit/1927115))
* **font.d.ts:** tryed adding import specification for build to pass ([34e8001](https://github.com/ElementAI/@eai-material-ui/commit/34e8001))
* **ThemeDemo.tsc:** themeDemo.tsx ([a396a46](https://github.com/ElementAI/@eai-material-ui/commit/a396a46))
* **ThemeDemo.tsx:** added missing typography ([a51af04](https://github.com/ElementAI/@eai-material-ui/commit/a51af04))
* **ThemeDemo.tsx:** updated to render components preview ([c28bd91](https://github.com/ElementAI/@eai-material-ui/commit/c28bd91))


### Features

* **ThemeDemo.tsx:** probs final look of the demo for now ([5550025](https://github.com/ElementAI/@eai-material-ui/commit/5550025))
* **updated Colors and default theme renders:** changed colors with doriane and new display for them ([bb2afac](https://github.com/ElementAI/@eai-material-ui/commit/bb2afac))
* **useGlobalStyles (hook):** added font loading to global sytyles hook ([fd84286](https://github.com/ElementAI/@eai-material-ui/commit/fd84286))





## [0.2.3](https://github.com/ElementAI/@eai-material-ui/compare/@eai-material-ui/docs@0.2.2...@eai-material-ui/docs@0.2.3) (2019-03-27)

**Note:** Version bump only for package @eai-material-ui/docs





## [0.2.2](https://github.com/ElementAI/@eai-material-ui/compare/@eai-material-ui/docs@0.2.1...@eai-material-ui/docs@0.2.2) (2019-03-26)

**Note:** Version bump only for package @eai-material-ui/docs





## 0.2.1 (2019-02-12)


### Bug Fixes

* sperating withThemeHooks and withThemeCore ([5838157](https://github.com/ElementAI/@eai-material-ui/commit/5838157))


### Features

* **build:** prepack script ([46e676f](https://github.com/ElementAI/@eai-material-ui/commit/46e676f))
* **build:** sorybook addons ([5fdf794](https://github.com/ElementAI/@eai-material-ui/commit/5fdf794))
* **demo:** dempInput render prop compoent ([67427d6](https://github.com/ElementAI/@eai-material-ui/commit/67427d6))
* **docs:** adding docs package pacakge to show case theme ([a6d8128](https://github.com/ElementAI/@eai-material-ui/commit/a6d8128))
* remove withTheme* HOC, as they are adding confusion ([908b2d6](https://github.com/ElementAI/@eai-material-ui/commit/908b2d6))
