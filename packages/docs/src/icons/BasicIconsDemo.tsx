import { Basic as BasicIcons } from '@eai-material-ui/icons';
import { useGlobalStyles } from '@eai-material-ui/theme';
import { Theme, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import * as React from 'react';

const useStyles = makeStyles((theme: Theme) => {
  return {
    container: {
      display: 'flex',
      flexDirection: 'row',
      flexFlow: 'row wrap',
      height: 'min-content',
      padding: theme.spacing.unit * 2,
    },
    iconContainer: {
      display: 'flex',
      margin: theme.spacing.unit * 2,
      flexDirection: 'column',
      alignItems: 'center',
      height: 'min-content',
      minWidth: theme.spacing.unit * 10,
      border: `1px solid ${theme.palette.divider}`,
      borderRadius: theme.shape.borderRadius,
    },
    icon: {
      margin: `${theme.spacing.unit * 2}px`,
    },
    label: {
      marginBottom: theme.spacing.unit,
      paddingLeft: theme.spacing.unit,
      paddingRight: theme.spacing.unit,
    },
  };
});

const BasicIconsDemo = () => {
  useGlobalStyles();
  const classes = useStyles();

  function renderIcons() {
    return Object.values(BasicIcons).map((Icon) => {
      // @ts-ignore
      const displayName = Icon.displayName;
      return (
        <div key={displayName} className={classes.iconContainer}>
          <Icon className={classes.icon} />
          <Typography
            color="textPrimary"
            className={classes.label}
            variant="caption"
          >
            {displayName}
          </Typography>
        </div>
      );
    });
  }

  return <div className={classes.container}>{renderIcons()}</div>;
};

export default BasicIconsDemo;
