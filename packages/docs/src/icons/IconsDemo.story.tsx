import { theme } from '@eai-material-ui/theme';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import { ThemeProvider } from '@material-ui/styles';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import BasicIconsDemo from './BasicIconsDemo';

storiesOf('Icons', module).add('Basic', () => (
  <ThemeProvider theme={theme}>
    <MuiThemeProvider theme={theme}>
      <BasicIconsDemo />
    </MuiThemeProvider>
  </ThemeProvider>
));
