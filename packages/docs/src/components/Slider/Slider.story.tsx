import { Slider } from '@eai-material-ui/components';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import React from 'react';

const markdown = `
This is a <Slider /> demo using hooks / useStyle() which is part of alpha package @material-ui/styles
  
  We have provide {theme} with both context providers
  * MuiThemeProvider from @material-ui/core to support withTheme() and withStyle() HOC
  * ThemeProvider from @material-ui/styles to support useStyle() hooks
`;

storiesOf('Components/Slider', module).add(
  'Slider',
  () => (
    <Slider
      label="Some Slide"
      value={2}
      min={0}
      max={10}
      color="secondary"
      onChange={action('onClick')}
    />
  ),
  { notes: { markdown } },
);
