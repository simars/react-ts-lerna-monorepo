import { Slider, sliderStyles } from '@eai-material-ui/components';
import { theme as defaultTheme } from '@eai-material-ui/theme';
import { MuiThemeProvider } from '@material-ui/core';
import { makeStyles, ThemeProvider } from '@material-ui/styles';
import { storiesOf } from '@storybook/react';
import React from 'react';
import { withMarkdown } from '../../storybooks';

import demo, { ControlFC, ControlProps } from '../demo/demo';

const Controls: ControlFC<number> = demo(2, 220);
const useStyles = makeStyles(sliderStyles, { withTheme: true });

const Render = (props?: any) => (
  <MuiThemeProvider theme={defaultTheme}>
    <ThemeProvider theme={defaultTheme}>
      <Controls>
        {({ value, onChange }: ControlProps<number>) => (
          <Slider
            label="Some Slide"
            value={value}
            min={0}
            max={10}
            onChange={onChange}
            classes={useStyles({ color: 'secondary' })}
            color="secondary"
          />
        )}
      </Controls>
    </ThemeProvider>
  </MuiThemeProvider>
);

const codeSnippet = `
  import { Slider, sliderStyles } from '@eai-material-ui/components';
  import { theme as defaultTheme } from '@eai-material-ui/theme';
  import { MuiThemeProvider } from '@material-ui/core';
  import { makeStyles, ThemeProvider } from '@material-ui/styles';
  import React from 'react';
  import demo, { ControlFC, ControlProps } from '../demo/demo';

  const Controls: ControlFC<number> = demo(2, 220);
  const useStyles = makeStyles(sliderStyles, { withTheme: true });
  
  const Render = (props?: any) => (
    <MuiThemeProvider theme={defaultTheme}>
      <ThemeProvider theme={defaultTheme}>
        <Controls>
          {({ value, onChange }: ControlProps<number>) => (
            <Slider
              label="Some Slide"
              value={value}
              min={0}
              max={10}
              onChange={onChange}
              classes={useStyles({ color: 'secondary' })}
              color="secondary"
            />
          )}
        </Controls>
      </ThemeProvider>
    </MuiThemeProvider>
  );

`;

const text = (code: string) => `

  ### Notes

  This is a <Slider /> demo using hooks / useStyle() which is part of alpha package @material-ui/styles
  
  We have provide {theme} with both context providers
  * MuiThemeProvider from @material-ui/core to support withTheme() and withStyle() HOC
  * ThemeProvider from @material-ui/styles to support useStyle() hooks
  
  
  ### Usage
  ~~~js
  ${code}
  ~~~`;

storiesOf('Components/Slider', module).add(
  'hooks demo',
  withMarkdown(text(codeSnippet))(() => <Render />),
);
