import React, { ChangeEvent, ChangeEventHandler, useState } from 'react';

type EventType = HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement;

type OnChange<T> = ChangeEventHandler<EventType>;

export interface ControlProps<T> {
  value: T;
  onChange: OnChange<T>;
}

export interface RenderControlProps<T> {
  children: React.FC<ControlProps<T>>;
}

export type ControlFC<T> = React.FC<RenderControlProps<T>>;

/**
 *
 * Demo component builder which expects a child (React.FC<{value, onChange}>) as render-prop
 * [value, setValue] = useState(initialValue); wires up to value & onChange props to control rendered component
 *
 * @param initialValue (initial value of state, of type T)
 * @param width width of the div that will wrap the demo component
 */
function demoInput(initialValue: string, width = 220): ControlFC<string> {
  return (props: RenderControlProps<string>) => {
    const [value, setValue] = useState(initialValue);
    const onChange = (e: ChangeEvent<EventType>) => setValue(e.target.value);
    return <div style={{ width }}>{props.children({ value, onChange })}</div>;
  };
}

export default demoInput;
