import React, { ChangeEvent, Fragment, useState } from 'react';

type OnChange<T> = (e: ChangeEvent<{}>, newVal: T) => void;

export interface ControlProps<T> {
  value: T;
  onChange: OnChange<T>;
}

export interface RenderControlProps<T> {
  children: React.FC<ControlProps<T>>;
}

export type ControlFC<T> = React.FC<RenderControlProps<T>>;

/**
 *
 * Demo component builder which expects a child (React.FC<{value, onChange}>) as render-prop
 * [value, setValue] = useState(initialValue); wires up to value & onChange props to control rendered component
 *
 * @param initialValue (initial value of state, of type T)
 * @param width width of the div that will wrap the demo component
 */
function demo<T>(initialValue: T, width = 220): ControlFC<T> {
  return (props: RenderControlProps<T>) => {
    const [value, setValue] = useState(initialValue);
    const onChange = (e: ChangeEvent<{}>, newVal: T) => setValue(newVal);
    return <div style={{ width }}>{props.children({ value, onChange })}</div>;
  };
}

export default demo;
