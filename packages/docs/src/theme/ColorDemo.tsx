import { Colors as colors } from '@eai-material-ui/theme';
import {
  createStyles,
  Theme,
  WithStyles,
  withStyles,
} from '@material-ui/core/styles';
import { Classes } from 'jss';
import * as React from 'react';

const hueColors = Object.keys(colors);
const hueValues = [
  50,
  100,
  200,
  300,
  400,
  500,
  600,
  700,
  800,
  900,
  'A100',
  'A200',
  'A400',
  'A700',
];

export const styles = (theme: Theme) => {
  return createStyles({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    name: {
      marginBottom: 60,
    },
    blockSpace: {
      height: 4,
    },
    colorContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    colorGroup: {
      padding: 0,
      margin: `0 ${theme.spacing.unit * 2}px ${theme.spacing.unit * 2}px 0`,
      flexGrow: 1,
      [theme.breakpoints.up('sm')]: {
        flexGrow: 0,
        width: '30%',
      },
    },
    colorValue: {
      ...theme.typography.caption,
      color: 'inherit',
      fontWeight: 'inherit',
    },
    body2: theme.typography.body2,
  });
};

function getColorBlock(
  classes: Classes,
  theme: Theme,
  colorName: string,
  colorValue: string | number,
  colorTitle: boolean = false,
) {
  // @ts-ignore
  const bgColor = colors[colorName][colorValue];
  const fgColor = theme.palette.getContrastText(bgColor);

  let blockTitle;
  if (colorTitle) {
    blockTitle = <div className={classes.name}>{colorName}</div>;
  }

  const rowStyle = {
    backgroundColor: bgColor,
    color: fgColor,
    listStyle: 'none',
    padding: 15,
  };

  if (bgColor === '#FFF') {
    return null;
  }

  return (
    <li style={rowStyle} key={colorValue} className={classes.body2}>
      {blockTitle}
      <div className={classes.colorContainer}>
        <span>{colorValue}</span>
        <span className={classes.colorValue}>{bgColor}</span>
      </div>
    </li>
  );
}

function getColorGroup(options: {
  classes: Classes;
  theme: Theme;
  color: string;
}) {
  const { classes, theme, color } = options;
  const cssColor = color
    .replace(' ', '')
    .replace(color.charAt(0), color.charAt(0).toLowerCase());

  const colorsList = hueValues.map((hueValue) =>
    getColorBlock(classes, theme, cssColor, hueValue),
  );

  return (
    <ul className={classes.colorGroup} key={cssColor}>
      {getColorBlock(classes, theme, cssColor, 500, true)}
      <div className={classes.blockSpace} />
      {colorsList}
    </ul>
  );
}

const ColorDemo: React.FC<ColorProps> = (props: ColorProps) => {
  const { classes, theme } = props;

  return (
    <div className={classes.root}>
      {hueColors.map((hueColor) =>
        getColorGroup({
          classes,
          theme,
          color: hueColor,
        }),
      )}
    </div>
  );
};

interface ColorProps extends WithStyles<typeof styles> {
  classes: Classes;
  theme: Theme;
}

export default withStyles(styles, { withTheme: true })(ColorDemo);
