import { darkTheme, theme } from '@eai-material-ui/theme';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import { ThemeProvider } from '@material-ui/styles';
import { storiesOf } from '@storybook/react';
import * as React from 'react';
import ThemeDemo from './ThemeDemo';

storiesOf('Theme', module).add('Light', () => (
  <ThemeProvider theme={theme}>
    <MuiThemeProvider theme={theme}>
      <ThemeDemo />
    </MuiThemeProvider>
  </ThemeProvider>
));

storiesOf('Theme', module).add('Dark', () => (
  <ThemeProvider theme={darkTheme}>
    <MuiThemeProvider theme={darkTheme}>
      <ThemeDemo />
    </MuiThemeProvider>
  </ThemeProvider>
));
