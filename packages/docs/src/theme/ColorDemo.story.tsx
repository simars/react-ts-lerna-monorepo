import { storiesOf } from '@storybook/react';
import * as React from 'react';
import ColorDemo from './ColorDemo';

storiesOf('Colors', module).add('Palettes', () => <ColorDemo />);
