import { useGlobalStyles } from '@eai-material-ui/theme';
import AppBar from '@material-ui/core/AppBar';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import CircularProgress from '@material-ui/core/CircularProgress';
import Divider from '@material-ui/core/Divider';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import FormLabel from '@material-ui/core/FormLabel';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import Paper from '@material-ui/core/Paper';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import { Theme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Tabs from '@material-ui/core/Tabs';
import TextField from '@material-ui/core/TextField';
import Toolbar from '@material-ui/core/Toolbar';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import DraftsIcon from '@material-ui/icons/Drafts';
import InboxIcon from '@material-ui/icons/Inbox';
import { makeStyles } from '@material-ui/styles';
import * as React from 'react';

import MenuIcon from '@material-ui/icons/Menu';

const useStyles = makeStyles((theme: Theme) => {
  return {
    root: {
      position: 'relative',
      paddingTop: theme.spacing.unit * 2,
      paddingBottom: theme.spacing.unit * 2,
    },
    appFrame: {
      display: 'flex',
      flexDirection: 'column',
      height: 'min-content',
      flexGrow: 1,
      position: 'relative',
      backgroundColor: theme.palette.background.default,
    },
    container: {
      display: 'flex',
      flexDirection: 'column',
      padding: theme.spacing.unit * 2,
    },
    componentDemo: {
      display: 'flex',
      flexDirection: 'column',
      padding: theme.spacing.unit,
      borderRadius: theme.shape.borderRadius,
    },
    paper: {
      padding: theme.spacing.unit * 1,
    },
    button: {
      margin: `${theme.spacing.unit}px 0px !important`,
      width: 'max-content',
    },
    iconButton: {
      margin: `${theme.spacing.unit}px 0px !important`,
      width: 'min-content',
    },
    input: {
      margin: theme.spacing.unit,
    },
    textField: {
      margin: `${theme.spacing.unit}px 0px !important`,
      width: theme.spacing.unit * 40,
    },
    textFieldMenu: {},
    formControl: {
      margin: `${theme.spacing.unit}px 0px !important`,
    },
    progress: {
      margin: theme.spacing.unit * 2,
    },
    avatar: {
      margin: `${theme.spacing.unit}px 0px !important`,
    },
    list: {
      margin: `${theme.spacing.unit}px 0px !important`,
    },
    table: {
      margin: `${theme.spacing.unit}px 0px !important`,
    },
  };
});

const ThemeDemo = () => {
  useGlobalStyles();
  const classes = useStyles();

  const options = [
    {
      value: 1,
      label: 'Option 1',
    },
    {
      value: 2,
      label: 'Option 2',
    },
    {
      value: 3,
      label: 'Option 3',
    },
    {
      value: 4,
      label: 'Option 4',
    },
  ];

  let id = 0;
  function createData(
    name: string,
    calories: number,
    fat: number,
    carbs: number,
    protein: number,
  ) {
    id += 1;
    return { id, name, calories, fat, carbs, protein };
  }

  const rows = [
    createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
    createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
    createData('Eclair', 262, 16.0, 24, 6.0),
    createData('Cupcake', 305, 3.7, 67, 4.3),
    createData('Gingerbread', 356, 16.0, 49, 3.9),
  ];

  const [tabValue, setTabValue] = React.useState<number>(0);

  function handleTabChange(_: React.ChangeEvent<{}>, newValue: number) {
    setTabValue(newValue);
  }

  function handlePageChange(
    _: React.MouseEvent<HTMLButtonElement, MouseEvent> | null,
    __: number,
  ) {
    return null;
  }

  return (
    <div className={classes.appFrame}>
      <div className={classes.container}>
        <Typography variant="h4" gutterBottom={true}>
          Typography
        </Typography>
        <div className={classes.componentDemo}>
          <Typography variant="h1" gutterBottom={true}>
            h1. Heading
          </Typography>
          <Typography variant="h2" gutterBottom={true}>
            h2. Heading
          </Typography>
          <Typography variant="h3" gutterBottom={true}>
            h3. Heading
          </Typography>
          <Typography variant="h4" gutterBottom={true}>
            h4. Heading
          </Typography>
          <Typography variant="body1" gutterBottom={true}>
            body1. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Quos blanditiis tenetur
          </Typography>
          <Typography variant="body2" gutterBottom={true}>
            body2. Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Quos blanditiis tenetur
          </Typography>
          <Typography variant="caption" gutterBottom={true}>
            caption. text
          </Typography>
          <Typography variant="button" gutterBottom={true}>
            button. text
          </Typography>
        </div>
      </div>

      <div className={classes.container}>
        <Typography variant="h4" gutterBottom={true}>
          App Bar
        </Typography>
        <div className={classes.componentDemo}>
          <AppBar position="static">
            <Toolbar>
              <IconButton color="inherit" aria-label="Menu">
                <MenuIcon />
              </IconButton>
              <Typography variant="h6" color="inherit">
                Application Title
              </Typography>
            </Toolbar>
          </AppBar>
        </div>
      </div>

      <div className={classes.container}>
        <Typography variant="h4" gutterBottom={true}>
          Paper
        </Typography>
        <div className={classes.componentDemo}>
          <Paper className={classes.paper}>
            <Typography variant="h5">This is a Paper</Typography>
            <Typography variant="body1">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos
              blanditiis tenetur
            </Typography>
          </Paper>
        </div>
      </div>

      <div className={classes.container}>
        <Typography variant="h4" gutterBottom={true}>
          Buttons
        </Typography>
        <div className={classes.componentDemo}>
          <Button
            color="primary"
            variant="contained"
            className={classes.button}
          >
            Contained Primary
          </Button>
          <Button
            color="secondary"
            variant="contained"
            className={classes.button}
          >
            Contained Secondary
          </Button>
          <Button color="primary" variant="outlined" className={classes.button}>
            Outlined Primary
          </Button>
          <Button
            color="secondary"
            variant="outlined"
            className={classes.button}
          >
            Outlined Secondary
          </Button>
          <Button color="primary" variant="text" className={classes.button}>
            Text Primary
          </Button>
          <Button color="secondary" variant="text" className={classes.button}>
            Text Secondary
          </Button>
          <IconButton color="primary" className={classes.iconButton}>
            <MenuIcon />
          </IconButton>
          <IconButton color="secondary" className={classes.iconButton}>
            <MenuIcon />
          </IconButton>
        </div>
      </div>

      <div className={classes.container}>
        <Typography variant="h4" gutterBottom={true}>
          Inputs
        </Typography>
        <div className={classes.componentDemo}>
          <TextField
            variant="outlined"
            className={classes.textField}
            label="Text"
          />
          <TextField
            select={true}
            label="Select"
            className={classes.textField}
            SelectProps={{
              MenuProps: {
                className: classes.textFieldMenu,
              },
            }}
            variant="outlined"
          >
            {options.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
          <FormControl className={classes.formControl}>
            <FormLabel> Radio </FormLabel>
            <RadioGroup name="radio">
              <FormControlLabel
                value="1"
                control={<Radio color="primary" />}
                label="Option 1"
              />
              <FormControlLabel
                value="2"
                control={<Radio />}
                label="Option 2"
              />
              <FormControlLabel
                value="3"
                disabled={true}
                control={<Radio />}
                label="Option 3 (Disabled)"
              />
            </RadioGroup>
          </FormControl>

          <FormControl className={classes.formControl}>
            <FormLabel> Checkbox </FormLabel>
            <FormGroup>
              <FormControlLabel
                value="1"
                control={<Checkbox color="primary" />}
                label="Option 1"
              />
              <FormControlLabel
                value="2"
                control={<Checkbox />}
                label="Option 2"
              />
              <FormControlLabel
                value="3"
                disabled={true}
                control={<Checkbox />}
                label="Option 3 (Disabled)"
              />
            </FormGroup>
          </FormControl>
        </div>
      </div>

      <div className={classes.container}>
        <Typography variant="h4" gutterBottom={true}>
          Circular Progress
        </Typography>
        <div className={classes.componentDemo}>
          <Typography variant="body1" gutterBottom={true}>
            Primary
          </Typography>
          <CircularProgress className={classes.progress} />
          <Typography variant="body1">Secondary</Typography>
          <CircularProgress className={classes.progress} color="secondary" />
        </div>
      </div>

      <div className={classes.container}>
        <Typography variant="h4" gutterBottom={true}>
          Avatar
        </Typography>
        <div className={classes.componentDemo}>
          <Avatar className={classes.avatar}>?</Avatar>
        </div>
      </div>

      <div className={classes.container}>
        <Typography variant="h4" gutterBottom={true}>
          Tooltip
        </Typography>
        <div className={classes.componentDemo}>
          <Tooltip title="This is a Tooltip">
            <Button variant="text" className={classes.button}>
              Hover me
            </Button>
          </Tooltip>
        </div>
      </div>

      <div className={classes.container}>
        <Typography variant="h4" gutterBottom={true}>
          List
        </Typography>
        <div className={classes.componentDemo}>
          <Paper className={classes.list}>
            <List component="nav">
              <ListItem button={true}>
                <ListItemIcon>
                  <InboxIcon />
                </ListItemIcon>
                <ListItemText primary="Inbox" />
              </ListItem>
              <ListItem button={true}>
                <ListItemIcon>
                  <DraftsIcon />
                </ListItemIcon>
                <ListItemText primary="Drafts" />
              </ListItem>
            </List>
            <Divider />
            <List component="nav">
              <ListItem button={true}>
                <ListItemText primary="Trash" />
              </ListItem>
              <ListItem button={true}>
                <ListItemText primary="Spam" />
              </ListItem>
            </List>
          </Paper>
        </div>
      </div>

      <div className={classes.container}>
        <Typography variant="h4" gutterBottom={true}>
          Table
        </Typography>
        <div className={classes.componentDemo}>
          <Paper className={classes.table}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Dessert (100g serving)</TableCell>
                  <TableCell align="right">Calories</TableCell>
                  <TableCell align="right">Fat (g)</TableCell>
                  <TableCell align="right">Carbs (g)</TableCell>
                  <TableCell align="right">Protein (g)</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {rows.map((row) => (
                  <TableRow hover={true} key={row.id}>
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell align="right">{row.calories}</TableCell>
                    <TableCell align="right">{row.fat}</TableCell>
                    <TableCell align="right">{row.carbs}</TableCell>
                    <TableCell align="right">{row.protein}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
            <TablePagination
              rowsPerPageOptions={[]}
              component="div"
              count={rows.length}
              rowsPerPage={5}
              page={0}
              backIconButtonProps={{
                'aria-label': 'Previous Page',
              }}
              nextIconButtonProps={{
                'aria-label': 'Next Page',
              }}
              onChangePage={handlePageChange}
            />
          </Paper>
        </div>
      </div>

      <div className={classes.container}>
        <Typography variant="h4" gutterBottom={true}>
          Tabs
        </Typography>
        <div className={classes.componentDemo}>
          <Paper>
            <Tabs
              value={tabValue}
              indicatorColor="primary"
              textColor="primary"
              onChange={handleTabChange}
            >
              <Tab value={0} label="Tab 1" />
              <Tab value={1} label="Tab 2" />
              <Tab value={2} label="Tab 3 (disabled)" disabled={true} />
            </Tabs>
          </Paper>
        </div>
      </div>
    </div>
  );
};

export default ThemeDemo;
