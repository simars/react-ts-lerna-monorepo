import * as React from 'react';
export interface LabelProps {
    labelWidth?: number;
}
export interface InputWithLabelProps extends LabelProps {
    id?: string;
    label?: string;
}
export interface InputWithoutLabelProps extends LabelProps {
    id: string;
    label: string;
}
export declare type InputLabelProps = InputWithLabelProps | InputWithoutLabelProps;
export interface InputProps {
    name?: string;
    type?: string;
}
export declare const Input: React.SFC<InputProps & InputLabelProps>;
