# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.6](https://github.com/ElementAI/@eai-material-ui/compare/@eai-material-ui/components@0.2.5...@eai-material-ui/components@0.2.6) (2019-04-18)

**Note:** Version bump only for package @eai-material-ui/components





## [0.2.5](https://github.com/ElementAI/@eai-material-ui/compare/@eai-material-ui/components@0.2.4...@eai-material-ui/components@0.2.5) (2019-04-01)

**Note:** Version bump only for package @eai-material-ui/components





## [0.2.4](https://github.com/ElementAI/@eai-material-ui/compare/@eai-material-ui/components@0.2.3...@eai-material-ui/components@0.2.4) (2019-03-29)

**Note:** Version bump only for package @eai-material-ui/components





## [0.2.3](https://github.com/ElementAI/@eai-material-ui/compare/@eai-material-ui/components@0.2.2...@eai-material-ui/components@0.2.3) (2019-03-27)

**Note:** Version bump only for package @eai-material-ui/components





## [0.2.2](https://github.com/ElementAI/@eai-material-ui/compare/@eai-material-ui/components@0.2.1...@eai-material-ui/components@0.2.2) (2019-03-26)

**Note:** Version bump only for package @eai-material-ui/components





## 0.2.1 (2019-02-12)


### Bug Fixes

* sperating withThemeHooks and withThemeCore ([5838157](https://github.com/ElementAI/@eai-material-ui/commit/5838157))


### Features

* **build:** prepack script ([46e676f](https://github.com/ElementAI/@eai-material-ui/commit/46e676f))
* **build:** sorybook addons ([5fdf794](https://github.com/ElementAI/@eai-material-ui/commit/5fdf794))
* **demo:** dempInput render prop compoent ([67427d6](https://github.com/ElementAI/@eai-material-ui/commit/67427d6))
* remove withTheme* HOC, as they are adding confusion ([908b2d6](https://github.com/ElementAI/@eai-material-ui/commit/908b2d6))
