import { createStyles, PropTypes } from '@material-ui/core';
import { StyledComponentProps, Theme } from '@material-ui/core/styles';
import { ClassNameMap } from '@material-ui/core/styles/withStyles';
import Typography from '@material-ui/core/Typography';
import MuiSlider, {
  SliderClassKey,
  SliderProps as MuiSliderProps,
} from '@material-ui/lab/Slider';
import * as React from 'react';

export interface SliderProps extends MuiSliderProps {
  /**
   * Classes for styling the slider.
   * @default useStyles ({color: 'secondary'})
   */
  classes?: StyledComponentProps<SliderClassKey | 'slider' | 'container'> &
    Partial<ClassNameMap<SliderClassKey | 'slider' | 'container'>>;

  /**
   * Theme used in styling palette (primary / secondary). expected to be provided
   *
   * @default useStyles ({color: 'secondary'})
   */
  theme?: Theme;

  /**
   * Color for label, value and slider line
   *
   */
  color?: PropTypes.Color;

  /**
   * Label describing the slider and value
   *
   *
   * @default "secondary"
   */
  label: string;
}

const styledBy = (
  property: string,
  mapping: { [x: string]: any; secondary?: string; primary?: string },
) => (props: { [x: string]: string | number }) => mapping[props[property]];

export const styles = (theme: Theme) => {
  return createStyles({
    slider: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      padding: '22px 0px',
      // @ts-ignore
      '& div:first-child': {
        backgroundColor: styledBy('color', {
          secondary: theme.palette.secondary.main,
          primary: theme.palette.primary.main,
        }),
      },
    },
  });
};

export const Slider = ({ color, label, classes, ...rest }: SliderProps) => {
  return (
    <React.Fragment>
      <Typography color={color} id="label">
        {label}
      </Typography>
      <Typography color={color} style={{ float: 'right' }} id="val">
        {rest.value}
      </Typography>
      <MuiSlider classes={{ container: classes && classes.slider }} {...rest} />
    </React.Fragment>
  );
};

export default Slider;
