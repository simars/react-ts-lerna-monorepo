import { createMuiTheme, Theme } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';
import Colors from './Colors';

const defaultTheme: Theme = createMuiTheme({});

const textPrimary = fade(Colors.purple[50], 1);
const textSecondary = fade(Colors.purple[50], 0.6);
const textDisabled = fade(Colors.purple[50], 0.4);
const textHint = fade(Colors.purple[50], 0.3);
const lightBorder = fade(Colors.purple[50], 0.16);

const highlightGreen = Colors.green[100];

const divider = Colors.darkGrey[900];

export const darkTheme: Theme = createMuiTheme({
  breakpoints: {
    keys: ['xs', 'sm', 'md', 'lg', 'xl'],
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1920,
    },
  },
  mixins: {
    toolbar: {
      minHeight: defaultTheme.spacing.unit * 7,
    },
  },
  palette: {
    type: 'light',
    primary: Colors.purple,
    secondary: Colors.grey,
    error: Colors.red,
    text: {
      primary: textPrimary,
      secondary: textSecondary,
      disabled: textDisabled,
      hint: textHint,
    },
    background: {
      paper: Colors.darkGrey[700],
      default: Colors.darkGrey[900],
    },
    divider,
    action: {
      active: textPrimary,
      hover: fade(Colors.grey[50], 0.08),
      hoverOpacity: 0.08,
      selected: fade(Colors.grey[50], 0.14),
      disabled: fade(Colors.grey[50], 0.26),
      disabledBackground: fade(Colors.grey[50], 0.12),
    },
  },
  shape: {
    borderRadius: 3,
  },
  typography: {
    fontFamily: "'Aktiv', 'Roboto', sans-serif",
    useNextVariants: true,
    fontSize: 16,
    fontWeightLight: 400,
    fontWeightRegular: 500,
    fontWeightMedium: 700,
    h1: {
      color: textPrimary,
      fontFamily: "'Aktiv', 'Roboto', sans-serif",
      fontWeight: 500,
      fontSize: '3rem',
      lineHeight: '1.3rem',
      letterSpacing: '0px',
    },
    h2: {
      color: textPrimary,
      fontFamily: "'Aktiv', 'Roboto', sans-serif",
      fontWeight: 500,
      fontSize: '2rem',
      lineHeight: '1.5rem',
      letterSpacing: '0px',
    },
    h3: {
      color: textPrimary,
      fontFamily: "'Aktiv', 'Roboto', sans-serif",
      fontWeight: 500,
      fontSize: '1.5rem',
      lineHeight: '1.5rem',
      letterSpacing: '0px',
    },
    h4: {
      color: textPrimary,
      fontFamily: "'Aktiv', 'Roboto', sans-serif",
      fontWeight: 500,
      fontSize: '1.2rem',
      lineHeight: '1.6rem',
      letterSpacing: '0px',
    },
    body1: {
      color: textPrimary,
      fontFamily: "'Aktiv', 'Roboto', sans-serif",
      fontWeight: 500,
      fontSize: '1rem',
      lineHeight: '1.7rem',
      letterSpacing: '0px',
    },
    body2: {
      color: textPrimary,
      fontFamily: "'Aktiv', 'Roboto', sans-serif",
      fontWeight: 500,
      fontSize: '0.875rem',
      lineHeight: '1.5rem',
      letterSpacing: '0px',
    },
    caption: {
      color: textPrimary,
      fontFamily: "'Aktiv', 'Roboto', sans-serif",
      fontWeight: 500,
      fontSize: '0.75rem',
      lineHeight: '1.66rem',
      letterSpacing: '0px',
    },
    button: {
      color: textPrimary,
      fontFamily: "'Aktiv', 'Roboto', sans-serif",
      fontWeight: 500,
      fontSize: '1rem',
      lineHeight: '1.7rem',
      letterSpacing: '0px',
    },
  },
  overrides: {
    MuiSvgIcon: {
      root: {
        fontSize: '20px',
      },
    },
    MuiPaper: {
      rounded: {
        boxShadow: `unset`,
      },
      elevation2: {
        boxShadow: `unset`,
      },
    },
    MuiIconButton: {
      root: {
        borderRadius: 3,
        padding: defaultTheme.spacing.unit,
      },
    },
    MuiAvatar: {
      root: {
        borderRadius: 3,
      },
      colorDefault: {
        backgroundColor: 'transparent',
        border: `2px solid ${lightBorder}`,
        fontSize: '12px',
        color: textPrimary,
        fontWeight: defaultTheme.typography.fontWeightMedium,
      },
    },
    MuiAppBar: {
      root: {
        boxShadow: 'unset',
      },
      colorDefault: {
        backgroundColor: Colors.darkGrey[900],
        color: textPrimary,
      },
    },
    MuiToolbar: {
      root: {
        marginRight: defaultTheme.spacing.unit * 4,
        marginLeft: defaultTheme.spacing.unit * 4,
      },
    },
    MuiChip: {
      root: {
        fontSize: '12px',
        color: textPrimary,
        fontWeight: defaultTheme.typography.fontWeightMedium,
        borderRadius: defaultTheme.shape.borderRadius,
      },
      outlined: {
        border: `2px solid ${lightBorder}`,
        borderRadius: defaultTheme.shape.borderRadius,
      },
    },
    MuiSelect: {
      icon: {
        color: textPrimary,
      },
    },
    MuiTable: {
      root: {
        tableLayout: 'fixed',
        position: 'relative',
      },
    },
    MuiTableHead: {
      root: {
        borderBottom: `2px solid ${divider}`,
      },
    },
    MuiTableRow: {
      root: {
        position: 'relative',
        height: defaultTheme.spacing.unit * 8,
        borderBottom: `2px solid ${divider}`,
        '&:last-child': {
          borderBottom: 'unset',
        },
        backgroundColor: Colors.darkGrey[800],
        '&$hover:hover': {
          backgroundColor: Colors.darkGrey[600],
        },
      },
      head: {
        height: defaultTheme.spacing.unit * 7,
        backgroundColor: Colors.darkGrey[700],
      },
      hover: {
        cursor: 'pointer',
      },
    },
    MuiTableCell: {
      root: {
        color: textPrimary,
        position: 'relative',
        padding: `${defaultTheme.spacing.unit * 0}px ${defaultTheme.spacing
          .unit * 2}px`,
        borderBottom: 'unset',
      },
      head: {
        color: textDisabled,
        paddingTop: 0,
        paddingBottom: 0,
        borderBottom: 'unset',
      },
      paddingCheckbox: {
        padding: `${defaultTheme.spacing.unit * 0}px ${defaultTheme.spacing
          .unit * 3}px`,
        borderBottom: 'unset',
      },
    },
    MuiBadge: {
      badge: {
        backgroundColor: Colors.grey[300],
        width: 20,
        height: 20,
      },
    },
    MuiList: {
      padding: {
        paddingTop: 0,
        paddingBottom: 0,
      },
    },
    MuiFormHelperText: {
      root: {
        marginBottom: defaultTheme.spacing.unit,
      },
    },
    MuiOutlinedInput: {
      root: {
        position: 'relative',
        borderWidth: 2,
        '& $notchedOutline': {
          borderColor: fade(Colors.purple[500], 0.6),
          borderWidth: 2,
        },
        '&:hover:not($disabled):not($focused):not($error) $notchedOutline': {
          borderColor: Colors.purple[500],
          // Reset on touch devices, it doesn't add specificity
          '@media (hover: none)': {
            borderColor: Colors.purple[500],
          },
        },
      },
    },
    MuiLinearProgress: {
      root: {
        height: 10,
        borderRadius: 10,
      },
      colorPrimary: {
        backgroundColor: fade(Colors.darkGrey[500], 0.6),
      },
      barColorPrimary: {
        backgroundColor: highlightGreen,
      },
    },
    MuiButton: {
      outlined: {
        padding: '5px 16px',
        border: `2px solid ${fade(Colors.grey[50], 0.4)}`,
        '&$disabled': {
          border: `2px solid ${textSecondary}`,
        },
      },
      /* Styles applied to the root element if `variant="outlined"` and `color="primary"`. */
      outlinedPrimary: {
        color: Colors.purple[500],
        border: `2px solid ${fade(Colors.purple[500], 0.6)}`,
        '&:hover': {
          border: `2px solid ${Colors.purple[500]}`,
          backgroundColor: fade(
            Colors.purple[500],
            defaultTheme.palette.action.hoverOpacity,
          ),
          // Reset on touch devices, it doesn't add specificity
          '@media (hover: none)': {
            backgroundColor: 'transparent',
          },
        },
      },
      /* Styles applied to the root element if `variant="outlined"` and `color="secondary"`. */
      outlinedSecondary: {
        color: Colors.grey[500],
        border: `2px solid ${fade(Colors.grey[500], 0.6)}`,
        '&:hover': {
          border: `2px solid ${Colors.grey[500]}`,
          backgroundColor: fade(
            Colors.grey[500],
            defaultTheme.palette.action.hoverOpacity,
          ),
          // Reset on touch devices, it doesn't add specificity
          '@media (hover: none)': {
            backgroundColor: 'transparent',
          },
        },
        '&$disabled': {
          border: `2px solid ${textDisabled}`,
        },
      },
    },
  },
});

export default darkTheme;
