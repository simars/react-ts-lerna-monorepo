import { makeStyles } from '@material-ui/styles';
import AktivWBdEot from './lib/AktivGrotesk_W_Bd.eot';
import AktivWBdWoff from './lib/AktivGrotesk_W_Bd.woff';
import AktivWBdWoff2 from './lib/AktivGrotesk_W_Bd.woff2';
import AktivWMdEot from './lib/AktivGrotesk_W_Md.eot';
import AktivWMdWoff from './lib/AktivGrotesk_W_Md.woff';
import AktivWMdWoff2 from './lib/AktivGrotesk_W_Md.woff2';
import AktivWRgEot from './lib/AktivGrotesk_W_Rg.eot';
import AktivWRgWoff from './lib/AktivGrotesk_W_Rg.woff';
import AktivWRgWoff2 from './lib/AktivGrotesk_W_Rg.woff2';

import AktivWBdItEot from './lib/AktivGrotesk_W_BdIt.eot';
import AktivWBdItWoff from './lib/AktivGrotesk_W_BdIt.woff';
import AktivWBdItWoff2 from './lib/AktivGrotesk_W_BdIt.woff2';
import AktivWMdItEot from './lib/AktivGrotesk_W_MdIt.eot';
import AktivWMdItWoff from './lib/AktivGrotesk_W_MdIt.woff';
import AktivWMdItWoff2 from './lib/AktivGrotesk_W_MdIt.woff2';
import AktivWRgItEot from './lib/AktivGrotesk_W_RgIt.eot';
import AktivWRgItWoff from './lib/AktivGrotesk_W_RgIt.woff';
import AktivWRgItWoff2 from './lib/AktivGrotesk_W_RgIt.woff2';
/**
 * Global styles for flexbox based layouts
 */

// @ts-ignore
export const useGlobalStyles = makeStyles({
  '@global': {
    html: {
      display: 'flex',
      height: '100%',
      flex: '1 1 auto',
    },
    body: {
      display: 'flex',
      flex: '1 1 auto',
    },
    '#root': {
      display: 'flex',
      flex: '1 1 auto',
    },
    a: {
      color: 'inherit',
      textDecoration: 'inherit',
    },
    '@font-face': [
      {
        fontFamily: 'Aktiv',
        src: `url(${AktivWRgEot}) format("embedded-opentype"), url(${AktivWRgEot}) format("embedded-opentype"),
        url(${AktivWRgWoff2}) format("woff2"),
        url(${AktivWRgWoff}) format("woff")`,
        fontWeight: 400,
        fontStyle: 'normal',
      },
      {
        fontFamily: 'Aktiv',
        src: `url(${AktivWMdEot}) format("embedded-opentype"), url(${AktivWMdEot}) format("embedded-opentype"),
        url(${AktivWMdWoff2}) format("woff2"),
        url(${AktivWMdWoff}) format("woff")`,
        fontWeight: 500,
        fontStyle: 'normal',
      },
      {
        fontFamily: 'Aktiv',
        src: `url(${AktivWBdEot}) format("embedded-opentype"), url(${AktivWBdEot}) format("embedded-opentype"),
        url(${AktivWBdWoff2}) format("woff2"),
        url(${AktivWBdWoff}) format("woff")`,
        fontWeight: 700,
        fontStyle: 'normal',
      },
      {
        fontFamily: 'Aktiv',
        src: `url(${AktivWRgItEot}) format("embedded-opentype"), url(${AktivWRgItEot}) format("embedded-opentype"),
        url(${AktivWRgItWoff2}) format("woff2"),
        url(${AktivWRgItWoff}) format("woff")`,
        fontWeight: 400,
        fontStyle: 'italic',
      },
      {
        fontFamily: 'Aktiv',
        src: `url(${AktivWMdItEot}) format("embedded-opentype"), url(${AktivWMdItEot}) format("embedded-opentype"),
        url(${AktivWMdItWoff2}) format("woff2"),
        url(${AktivWMdItWoff}) format("woff")`,
        fontWeight: 500,
        fontStyle: 'italic',
      },
      {
        fontFamily: 'Aktiv',
        src: `url(${AktivWBdItEot}) format("embedded-opentype"), url(${AktivWBdItEot}) format("embedded-opentype"),
        url(${AktivWBdItWoff2}) format("woff2"),
        url(${AktivWBdItWoff}) format("woff")`,
        fontWeight: 700,
        fontStyle: 'italic',
      },
    ],
  },
});
