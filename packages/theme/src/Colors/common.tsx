import { Color } from '@material-ui/core';

/**
 * EAI Shades for every hue; From Lightest to Darkest
 */
export interface Shades {
  50: string;
  100: string;
  200: string;
  300: string;
  400: string;
  500: string;
  600: string;
  700: string;
  800: string;
  900: string;
  ['A100']?: string;
  ['A200']?: string;
  ['A400']?: string;
  ['A700']?: string;
}

/**
 * @param shades; For A hue From Lightest to Darkest
 * @return Color: material-ui Color, with shade 50=white, and accents A100, A200, A400, A700 using 200, 400, 600, 700
 */
export const materialize = (shades: Shades): Color => {
  const { 200: A100, 400: A200, 600: A400, 800: A700 } = shades;
  return { A100, A200, A400, A700, ...shades };
};

const white = '#FFF';

export const calcium: Color = materialize({
  50: white,
  100: white,
  200: white,
  300: white, // light primary
  400: white,
  500: white, // main primary
  600: white,
  700: white, // dark primary
  800: white,
  900: white,
  ['A100']: white,
  ['A200']: white,
  ['A400']: white,
  ['A700']: white,
});
