import { materialize, Shades } from './common';

const purple: Shades = {
  50: '#F5F0FF',
  100: '#EEE9F7',
  200: '#FFF',
  300: '#9675E1',
  400: '#FFF',
  500: '#714BBD',
  600: '#FFF',
  700: '#351761',
  800: '#FFF',
  900: '#FFF',
  ['A100']: '#C7B4F5',
  ['A200']: '#714BBD',
  ['A400']: '#351761',
  ['A700']: '#1F0B3C',
};

export default materialize(purple);
