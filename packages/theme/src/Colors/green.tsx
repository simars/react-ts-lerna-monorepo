import { materialize, Shades } from './common';

const green: Shades = {
  50: '#E3F1E9',
  100: '#67C779',
  200: '#FFF',
  300: '#3CBB53',
  400: '#FFF',
  500: '#148E45',
  600: '#FFF',
  700: '#00783F',
  800: '#FFF',
  900: '#FFF',
};

export default materialize(green);
