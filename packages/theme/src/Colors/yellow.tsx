import { materialize, Shades } from './common';

const yellow: Shades = {
  50: '#FEF7E9',
  100: '#FFF',
  200: '#FFF',
  300: '#FFD073',
  400: '#FFF',
  500: '#F7B545',
  600: '#FFF',
  700: '#EBA926',
  800: '#FFF',
  900: '#FFF',
};

export default materialize(yellow);
