import { materialize, Shades } from './common';

const grey: Shades = {
  50: '#FFFFFF',
  100: '#F3F3F3',
  200: '#EAEBEB',
  300: '#DFE0E1',
  400: '#CACCCD',
  500: '#B5B7B9',
  600: '#A0A2A5',
  700: '#58595B',
  800: '#231F20',
  900: '#000000',
};

export default materialize(grey);
