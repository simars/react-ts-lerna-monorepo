import { default as blue } from './blue';
import { default as darkGrey } from './darkGrey';
import { default as green } from './green';
import { default as grey } from './grey';
import { default as orange } from './orange';
import { default as purple } from './purple';
import { default as red } from './red';
import { default as yellow } from './yellow';

export default {
  grey,
  darkGrey,
  yellow,
  green,
  orange,
  red,
  blue,
  purple,
};
