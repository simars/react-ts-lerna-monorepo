import { materialize, Shades } from './common';

const darkGrey: Shades = {
  50: '#DEDBE6',
  100: '#AFABB8',
  200: '#95919E',
  300: '#757080',
  400: '#666173',
  500: '#544F61',
  600: '#322D3D',
  700: '#25212E',
  800: '#211D29',
  900: '#16131F',
};

export default materialize(darkGrey);
