import { materialize, Shades } from './common';

const orange: Shades = {
  50: '#FFF0E8',
  100: '#FFF',
  200: '#FFF',
  300: '#FF9666',
  400: '#FFF',
  500: '#FF7F41',
  600: '#FFF',
  700: '#E9703B',
  800: '#FFF',
  900: '#FFF',
};

export default materialize(orange);
