import { materialize, Shades } from './common';

const red: Shades = {
  50: '#F8E5E9',
  100: '#FFF',
  200: '#FFF',
  300: '#EA6685',
  400: '#FFF',
  500: '#D84769',
  600: '#FFF',
  700: '#C3254A',
  800: '#FFF',
  900: '#FFF',
};

export default materialize(red);
