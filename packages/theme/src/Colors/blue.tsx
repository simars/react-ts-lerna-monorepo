import { materialize, Shades } from './common';

const blue: Shades = {
  50: '#E0F2F6',
  100: '#FFF',
  200: '#FFF',
  300: '#00A9CE',
  400: '#FFF',
  500: '#0095B8',
  600: '#FFF',
  700: '#0080A3',
  800: '#FFF',
  900: '#FFF',
};

export default materialize(blue);
