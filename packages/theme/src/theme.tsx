import { createMuiTheme, Theme } from '@material-ui/core/styles';
import Colors from './Colors';

const defaultTheme: Theme = createMuiTheme({});

export const theme: Theme = createMuiTheme({
  breakpoints: {
    keys: ['xs', 'sm', 'md', 'lg', 'xl'],
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1920,
    },
  },
  mixins: {
    toolbar: {
      minHeight: defaultTheme.spacing.unit * 7,
    },
  },
  palette: {
    secondary: Colors.grey,
    primary: Colors.purple,
    error: Colors.red,
  },
  shape: {
    borderRadius: 3,
  },
  typography: {
    fontFamily: "'Aktiv', 'Roboto', sans-serif",
    useNextVariants: true,
    fontSize: 16,
    fontWeightLight: 400,
    fontWeightRegular: 500,
    fontWeightMedium: 700,
    h1: {
      color: defaultTheme.palette.text.primary,
      fontFamily: "'Aktiv', 'Roboto', sans-serif",
      fontWeight: 500,
      fontSize: '3rem',
      lineHeight: '1.3rem',
      letterSpacing: '0px',
    },
    h2: {
      color: defaultTheme.palette.text.primary,
      fontFamily: "'Aktiv', 'Roboto', sans-serif",
      fontWeight: 500,
      fontSize: '2rem',
      lineHeight: '1.5rem',
      letterSpacing: '0px',
    },
    h3: {
      color: defaultTheme.palette.text.primary,
      fontFamily: "'Aktiv', 'Roboto', sans-serif",
      fontWeight: 500,
      fontSize: '1.5rem',
      lineHeight: '1.5rem',
      letterSpacing: '0px',
    },
    h4: {
      color: defaultTheme.palette.text.primary,
      fontFamily: "'Aktiv', 'Roboto', sans-serif",
      fontWeight: 500,
      fontSize: '1.2rem',
      lineHeight: '1.6rem',
      letterSpacing: '0px',
    },
    body1: {
      color: defaultTheme.palette.text.primary,
      fontFamily: "'Aktiv', 'Roboto', sans-serif",
      fontWeight: 500,
      fontSize: '1rem',
      lineHeight: '1.7rem',
      letterSpacing: '0px',
    },
    body2: {
      color: defaultTheme.palette.text.primary,
      fontFamily: "'Aktiv', 'Roboto', sans-serif",
      fontWeight: 500,
      fontSize: '0.875rem',
      lineHeight: '1.5rem',
      letterSpacing: '0px',
    },
    caption: {
      color: defaultTheme.palette.text.primary,
      fontFamily: "'Aktiv', 'Roboto', sans-serif",
      fontWeight: 500,
      fontSize: '0.75rem',
      lineHeight: '1.66rem',
      letterSpacing: '0px',
    },
    button: {
      color: defaultTheme.palette.text.primary,
      fontFamily: "'Aktiv', 'Roboto', sans-serif",
      fontWeight: 500,
      fontSize: '1rem',
      lineHeight: '1.7rem',
      letterSpacing: '0px',
    },
  },
  overrides: {
    MuiSvgIcon: {
      root: {
        fontSize: '20px',
      },
    },
    MuiPaper: {
      elevation2: {
        boxShadow: 'unset',
      },
    },
    MuiIconButton: {
      root: {
        borderRadius: 3,
      },
    },
    MuiAvatar: {
      root: {
        borderRadius: 3,
      },
    },
  },
});

export default theme;
