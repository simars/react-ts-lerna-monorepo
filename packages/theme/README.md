# EAI Theme
The EAI Theme is based on the Design teams guidelines, which can be found [here](https://drive.google.com/drive/folders/1JHR1dLc5QdTUfVFJBDlA3y-iap9xd0uZ)

## Colors
![Design Colors](docs/EAI-Design-colors.png?raw=true)

A `Color` is defined as an object with keys that match HTML font weights.

```Typescript
export interface Color {
  100: string;
  200: string;
  300: string;
  400: string;
  500: string;
  600: string;
  700: string;
  800: string;
  900: string;
}
```

`100` Corresponds to the lightest weights, and `900` to the darkest. `500` is the default weight should be uses as `main` in Material-UI theming. See `theme.tsx` for examples.
