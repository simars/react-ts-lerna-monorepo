# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.6](https://github.com/ElementAI/@eai-material-ui/compare/@eai-material-ui/theme@0.2.5...@eai-material-ui/theme@0.2.6) (2019-04-18)


### Bug Fixes

* **Colors:** capitalized colors ([7d9dda0](https://github.com/ElementAI/@eai-material-ui/commit/7d9dda0))
* **darkTheme:** fixed emptyRoot issue ([9bcc91e](https://github.com/ElementAI/@eai-material-ui/commit/9bcc91e))
* **Fixed lint errors:** lint errors resolved ([c2d5ee4](https://github.com/ElementAI/@eai-material-ui/commit/c2d5ee4))
* **lint:** fixed linting errors ([5afbaca](https://github.com/ElementAI/@eai-material-ui/commit/5afbaca))
* **theme:** adjusted frontWeight ([e10771d](https://github.com/ElementAI/@eai-material-ui/commit/e10771d))


### Features

* **Created Icons packages:** added Full Basic Icons suite ([e34af7e](https://github.com/ElementAI/@eai-material-ui/commit/e34af7e))
* **darkTheme and colors:** adjusted colors and fixed dark theme to match comments ([9869628](https://github.com/ElementAI/@eai-material-ui/commit/9869628))
* **icons:** fixed icons and typographie ([6ded25b](https://github.com/ElementAI/@eai-material-ui/commit/6ded25b))
* **linting:** fixed linting errors ([07ca36e](https://github.com/ElementAI/@eai-material-ui/commit/07ca36e))
* **theme:** added dark theme and demo ([a9a3410](https://github.com/ElementAI/@eai-material-ui/commit/a9a3410))





## [0.2.5](https://github.com/ElementAI/@eai-material-ui/compare/@eai-material-ui/theme@0.2.4...@eai-material-ui/theme@0.2.5) (2019-04-01)


### Bug Fixes

* **added typography to theme:** added h1-h4 specs ([e9548a0](https://github.com/ElementAI/@eai-material-ui/commit/e9548a0))
* **colors, theme:** changed colors again and updated theme ([d060af7](https://github.com/ElementAI/@eai-material-ui/commit/d060af7))
* **fixed font imports:** added babel config option to copy non processed files ([9061df2](https://github.com/ElementAI/@eai-material-ui/commit/9061df2))
* **theme.tsx:** added some overrides to theme ([66bc306](https://github.com/ElementAI/@eai-material-ui/commit/66bc306))





## [0.2.4](https://github.com/ElementAI/@eai-material-ui/compare/@eai-material-ui/theme@0.2.3...@eai-material-ui/theme@0.2.4) (2019-03-29)


### Bug Fixes

* **fixed color name:** changed name ([e3d2545](https://github.com/ElementAI/@eai-material-ui/commit/e3d2545))
* **font.d.ts:** finally got cmd working ([1927115](https://github.com/ElementAI/@eai-material-ui/commit/1927115))


### Features

* **useGlobalStyles (hook):** added font loading to global sytyles hook ([fd84286](https://github.com/ElementAI/@eai-material-ui/commit/fd84286))





## [0.2.3](https://github.com/ElementAI/@eai-material-ui/compare/@eai-material-ui/theme@0.2.2...@eai-material-ui/theme@0.2.3) (2019-03-27)

**Note:** Version bump only for package @eai-material-ui/theme





## [0.2.2](https://github.com/ElementAI/@eai-material-ui/compare/@eai-material-ui/theme@0.2.1...@eai-material-ui/theme@0.2.2) (2019-03-26)


### Bug Fixes

* **theme:** remove unused default themeing ([583b2a4](https://github.com/ElementAI/@eai-material-ui/commit/583b2a4))


### Features

* **theme:** create useGlobalStyles hook ([60bfefb](https://github.com/ElementAI/@eai-material-ui/commit/60bfefb))


### BREAKING CHANGES

* **theme:** Global styles now added using hook





## 0.2.1 (2019-02-12)


### Bug Fixes

* fixes to build, imports, documetnation ([79b190d](https://github.com/ElementAI/@eai-material-ui/commit/79b190d))
* sperating withThemeHooks and withThemeCore ([5838157](https://github.com/ElementAI/@eai-material-ui/commit/5838157))


### Features

* **build:** adding tsconfig.build.json for package theme that extends from root tscfonfigs ([2a1b886](https://github.com/ElementAI/@eai-material-ui/commit/2a1b886))
* **build:** prepack script ([46e676f](https://github.com/ElementAI/@eai-material-ui/commit/46e676f))
* **build:** sorybook addons ([5fdf794](https://github.com/ElementAI/@eai-material-ui/commit/5fdf794))
* **build:** yarn package for theme ([5761ee1](https://github.com/ElementAI/@eai-material-ui/commit/5761ee1))
* **demo:** dempInput render prop compoent ([67427d6](https://github.com/ElementAI/@eai-material-ui/commit/67427d6))
* **docs:** adding docs package pacakge to show case theme ([a6d8128](https://github.com/ElementAI/@eai-material-ui/commit/a6d8128))
* **theme:** create material ui theme ([333929d](https://github.com/ElementAI/@eai-material-ui/commit/333929d))
* remove withTheme* HOC, as they are adding confusion ([908b2d6](https://github.com/ElementAI/@eai-material-ui/commit/908b2d6))
* simple input component with / without label, themed eai ([5d3e165](https://github.com/ElementAI/@eai-material-ui/commit/5d3e165))
