# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.1.0](https://github.com/ElementAI/@eai-material-ui/compare/@eai-material-ui/icons@0.0.1...@eai-material-ui/icons@0.1.0) (2019-05-06)


### Bug Fixes

* **fixed icons:** some icons had stroke instead of fill ([bf35cb9](https://github.com/ElementAI/@eai-material-ui/commit/bf35cb9))
* **ix lin:** fixed lnterrors ([c30b85c](https://github.com/ElementAI/@eai-material-ui/commit/c30b85c))


### Features

* **PR fixes:** remvoes errors and id on svgs ([4389118](https://github.com/ElementAI/@eai-material-ui/commit/4389118))





## 0.0.1 (2019-04-18)


### Bug Fixes

* **icons:** removed private to repo ([94a63a3](https://github.com/ElementAI/@eai-material-ui/commit/94a63a3))


### Features

* **Created Icons packages:** added Full Basic Icons suite ([e34af7e](https://github.com/ElementAI/@eai-material-ui/commit/e34af7e))
* **icons:** fixed icons and typographie ([6ded25b](https://github.com/ElementAI/@eai-material-ui/commit/6ded25b))
