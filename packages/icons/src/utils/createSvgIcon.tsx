import SvgIcon, { SvgIconProps } from '@material-ui/core/SvgIcon';
import React from 'react';

export default function createSvgIcon(path: JSX.Element, displayName: string) {
  const IconBase: React.ComponentType<SvgIconProps> = (props: SvgIconProps) => (
    <SvgIcon viewBox="0 0 20 20" height="20px" width="20px" {...props}>
      {path}
    </SvgIcon>
  );

  const Icon = React.memo(IconBase);

  Icon.displayName = displayName;

  return Icon;
}
