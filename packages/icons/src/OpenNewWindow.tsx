import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <path
      d="M20,11 L20,1 C20,0.448 19.552,0 19,0 L9,0 L9,2 L16.586,2 L6.5,12.086 L6.5,13.5 L7.914,13.5 L18,3.414 L18,11 L20,11 Z"
      id="Fill-1"
    />
    <path d="M18,19 L18,18 L2,18 L1.971,2 L1,2 C0.448,2 0,2.448 0,3 L0,19 C0,19.552 0.448,20 1,20 L17,20 C17.552,20 18,19.552 18,19" />
  </React.Fragment>,
  'OpenNewWindow',
);
