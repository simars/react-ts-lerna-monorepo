import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <polygon points="0 9.0002 0.002 11.0012 11.599 10.9882 12.587 10.0002 11.575 8.9882" />
    <path d="M19.708,9.2935 L11.223,0.8085 L9.809,2.2225 L17.587,10.0005 L9.809,17.7785 L11.223,19.1925 L19.708,10.7075 C20.099,10.3165 20.099,9.6845 19.708,9.2935" />
  </React.Fragment>,
  'Go',
);
