import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <polygon points="9.9999 8.5862 1.7069 0.2922 0.2939 1.7062 8.5859 10.0002 0.2939 18.2922 1.7069 19.7062 9.9999 11.4142 18.2939 19.7062 19.7069 18.2922 11.4139 10.0002 19.7069 1.7062 18.2939 0.2922" />
  </React.Fragment>,
  'Close',
);
