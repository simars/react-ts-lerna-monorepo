import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <polygon points="14.5 11.5 17.5 11.5 17.5 8.5 14.5 8.5" />
    <polygon points="11.4995 11.5 8.4995 11.5 8.4995 8.5 11.4995 8.5" />
    <polygon points="2.5 11.5 5.5 11.5 5.5 8.5 2.5 8.5" />
  </React.Fragment>,
  'MenuMore',
);
