import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <polygon points="0 5 20 5 20 3 0 3" />
    <polygon points="0 11 20 11 20 9 0 9" />
    <polygon points="0 17 20 17 20 14.999 0 14.999" />
  </React.Fragment>,
  'Menu',
);
