import { default as Add } from './Add';
import { default as AddCircle } from './AddCircle';
import { default as Attachement } from './Attachement';
import { default as Back } from './Back';
import { default as Calendar } from './Calendar';
import { default as Cards } from './Cards';
import { default as Check } from './Check';
import { default as CheckCircle } from './CheckCircle';
import { default as Close } from './Close';
import { default as Comment } from './Comment';
import { default as Edit } from './Edit';
import { default as Filter } from './Filter';
import { default as FullScreen } from './FullScreen';
import { default as Go } from './Go';
import { default as Information } from './Information';
import { default as Less } from './Less';
import { default as List } from './List';
import { default as Location } from './Location';
import { default as Lock } from './Lock';
import { default as Menu } from './Menu';
import { default as MenuMore } from './MenuMore';
import { default as MoreCircle } from './MoreCircle';
import { default as Next } from './Next';
import { default as Notification } from './Notification';
import { default as OpenNewWindow } from './OpenNewWindow';
import { default as Rates } from './Rates';
import { default as Search } from './Search';
import { default as Settings } from './Settings';
import { default as Sort } from './Sort';
import { default as Truck } from './Truck';
import { default as Unlock } from './Unlock';
import { default as Verify } from './Verify';
import { default as Versions } from './Versions';
import { default as WarningCircle } from './WarningCircle';
import { default as WarningTriangle } from './WarningTriangle';

export const Basic = {
  Add,
  AddCircle,
  Attachement,
  Back,
  Calendar,
  Cards,
  Check,
  CheckCircle,
  Close,
  Comment,
  Edit,
  Filter,
  FullScreen,
  Go,
  Information,
  Less,
  List,
  Location,
  Lock,
  Menu,
  MenuMore,
  MoreCircle,
  Next,
  Notification,
  OpenNewWindow,
  Rates,
  Search,
  Settings,
  Sort,
  Truck,
  Unlock,
  Verify,
  Versions,
  WarningCircle,
  WarningTriangle,
};
