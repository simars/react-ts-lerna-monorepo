import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <path d="M6.9995,15.5864 L1.7025,10.2964 L0.2895,11.7114 L6.2935,17.7074 C6.4885,17.9024 6.7445,18.0004 6.9995,18.0004 C7.2555,18.0004 7.5115,17.9024 7.7075,17.7074 L19.7075,5.7074 L18.2925,4.2934 L6.9995,15.5864 Z" />
  </React.Fragment>,
  'Check',
);
