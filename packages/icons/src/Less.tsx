import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <polygon points="0.002 11.005 20 11.005 20 9.005 0.002 9.005" />
  </React.Fragment>,
  'Less',
);
