import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <polygon points="1.5867 13.4993 0.1827 19.8173 6.5007 18.4133 19.7077 5.2063 18.2937 3.7933 5.4987 16.5873 2.8167 17.1833 3.4127 14.5013 16.2067 1.7063 14.7937 0.2923" />
  </React.Fragment>,
  'Edit',
);
