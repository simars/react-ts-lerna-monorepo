import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <path
      d="M13,2 L18,2 L18,7 L20,7 L20,1 C20,0.448 19.552,0 19,0 L13,0 L13,2 Z"
      id="Fill-1"
    />
    <path d="M2,2 L7,2 L7,0 L1,0 C0.448,0 0,0.448 0,1 L0,7 L2,7 L2,2 Z" />
    <path
      d="M0,19 C0,19.552 0.448,20 1,20 L7,20 L7,18 L2,18 L2,13 L0,13 L0,19 Z"
      id="Fill-5"
    />
    <path d="M13,20 L19,20 C19.552,20 20,19.552 20,19 L20,13 L18,13 L18,18 L13,18 L13,20 Z" />
  </React.Fragment>,
  'FullScreen',
);
