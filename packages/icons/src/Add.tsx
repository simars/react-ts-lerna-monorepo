import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <polygon points="9 0 9 9 0 9 0 11 9 11 9 20 11 20 11 11 20 11 20 9 11 9 11 0" />
  </React.Fragment>,
  'Add',
);
