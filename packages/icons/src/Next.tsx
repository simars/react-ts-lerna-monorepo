import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <path d="M6.707,19.707 L15.707,10.707 C16.098,10.316 16.098,9.684 15.707,9.293 L6.707,0.293 L5.293,1.707 L13.586,10 L5.293,18.293 L6.707,19.707 Z" />
  </React.Fragment>,
  'Next',
);
