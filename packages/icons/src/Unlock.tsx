import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <path
      fillRule="evenodd"
      d="M2,18 L14,18 L14,10 L2,10 L2,18 Z M18,0 C15.243,0 13,2.243 13,5 L13,8 L1,8 C0.448,8 0,8.448 0,9 L0,19 C0,19.552 0.448,20 1,20 L15,20 C15.552,20 16,19.552 16,19 L16,9 C16,8.448 15.552,8 15,8 L15,8 L15,5 C15,3.346 16.346,2 18,2 C19.654,2 21,3.346 21,5 L21,8 L23,8 L23,5 C23,2.243 20.757,0 18,0 L18,0 Z"
      id="Fill-1"
    />
    <path d="M8,15.5 C8.829,15.5 9.5,14.829 9.5,14 C9.5,13.172 8.829,12.5 8,12.5 C7.172,12.5 6.5,13.172 6.5,14 C6.5,14.829 7.172,15.5 8,15.5" />
  </React.Fragment>,
  'Unlock',
);
