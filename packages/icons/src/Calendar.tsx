import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <path
      fillRule="evenodd"
      d="M2,18 L18,18 L18,9 L2,9 L2,18 Z M2,5 C2,4.449 2.449,4 3,4 L5,4 L5,7 L2,7 L2,5 Z M7,7 L13,7 L13,4 L7,4 L7,7 Z M17,4 C17.551,4 18,4.449 18,5 L18,7 L15,7 L15,4 L17,4 Z M17,2 L15,2 L15,0 L13,0 L13,2 L7,2 L7,0 L5,0 L5,2 L3,2 C1.346,2 0,3.346 0,5 L0,19 C0,19.552 0.448,20 1,20 L19,20 C19.552,20 20,19.552 20,19 L20,5 C20,3.346 18.654,2 17,2 L17,2 Z"
    />
  </React.Fragment>,
  'Calendar',
);
