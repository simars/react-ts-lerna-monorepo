import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <polygon points="5 4.44444444 19.999 4.44444444 19.999 2.22222222 5 2.22222222" />
    <polygon points="0 4.44444444 2 4.44444444 2 2.22222222 0 2.22222222" />
    <polygon points="0 11.1111111 2 11.1111111 2 8.88888889 0 8.88888889" />
    <polygon
      id="Fill-5"
      points="0 17.7777778 2 17.7777778 2 15.5544444 0 15.5544444"
    />
    <polygon points="5 11.1111111 19.999 11.1111111 19.999 8.88888889 5 8.88888889" />
    <polygon points="5 17.7777778 19.999 17.7777778 19.999 15.5544444 5 15.5544444" />
  </React.Fragment>,
  'List',
);
