import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <polygon points="0 5 20 5 20 3 0 3" />
    <polygon points="0 11 13 11 13 9 0 9" />
    <polygon points="0 17 6 17 6 15 0 15" />
  </React.Fragment>,
  'Sort',
);
