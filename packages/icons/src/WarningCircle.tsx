import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <path
      d="M10,2 C14.411,2 18,5.589 18,10 C18,14.411 14.411,18 10,18 C5.589,18 2,14.411 2,10 C2,5.589 5.589,2 10,2 M10,20 C15.514,20 20,15.514 20,10 C20,4.486 15.514,0 10,0 C4.486,0 0,4.486 0,10 C0,15.514 4.486,20 10,20"
      id="Fill-1"
    />
    <polygon points="11 6.0005 9 5.9995 8.999 9.9995 10.999 10.0005" />
    <polygon id="Fill-5" points="9 14 11 14 11 12 9 12" />
  </React.Fragment>,
  'WarningCircle',
);
