import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <path
      d="M9.793,13.2071 C10.184,13.5981 10.816,13.5981 11.207,13.2071 L18,6.4141 L18,9.2431 L20,9.2431 L20,3.0001 L13.757,3.0001 L13.757,5.0001 L16.586,5.0001 L10.5,11.0861 L7.207,7.7931 C6.816,7.4021 6.184,7.4021 5.793,7.7931 L0,13.5861 L0,16.4141 L6.5,9.9141 L9.793,13.2071 Z"
      id="Fill-1"
    />
  </React.Fragment>,
  'Versions',
);
