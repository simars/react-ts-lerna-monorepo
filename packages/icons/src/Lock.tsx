import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <path
      fillRule="evenodd"
      d="M7,8 L7,5 C7,3.346 8.346,2 10,2 C11.654,2 13,3.346 13,5 L13,8 L7,8 Z M4,18 L16,18 L16,10 L4,10 L4,18 Z M10,0 C7.243,0 5,2.243 5,5 L5,8 L3,8 C2.448,8 2,8.448 2,9 L2,19 C2,19.552 2.448,20 3,20 L17,20 C17.552,20 18,19.552 18,19 L18,9 C18,8.448 17.552,8 17,8 L15,8 L15,5 C15,2.243 12.757,0 10,0 L10,0 Z"
      id="Fill-1"
    />
    <path d="M10,15.5 C10.829,15.5 11.5,14.829 11.5,14 C11.5,13.172 10.829,12.5 10,12.5 C9.171,12.5 8.5,13.172 8.5,14 C8.5,14.829 9.171,15.5 10,15.5" />
  </React.Fragment>,
  'Lock',
);
