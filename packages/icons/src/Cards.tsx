import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <path
      d="M2,18 L14,18 C15.103,18 16,17.103 16,16 L2,16 L2,7 C0.897,7 0,7.897 0,9 L0,16 C0,17.103 0.897,18 2,18"
      id="Fill-1"
    />
    <path
      fillRule="evenodd"
      d="M6,6.5 L18,6.5 L18,5 L6,5 L6,6.5 Z M18,12 L6,12.001 L6,8.5 L18,8.5 L18,12 Z M6,14 L18,14 C19.103,14 20,13.103 20,12 L20,5 C20,3.897 19.103,3 18,3 L6,3 C4.897,3 4,3.897 4,5 L4,12 C4,13.103 4.897,14 6,14 L6,14 Z"
    />
  </React.Fragment>,
  'Cards',
);
