import React from 'react';
import createSvgIcon from './utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <polygon points="8.4015 9.02 7.4145 10.008 8.4265 11.02 20.0005 11.008 19.9985 9.007" />
    <path d="M8.7783,19.2002 L10.1923,17.7862 L2.4143,10.0082 L10.1923,2.2292 L8.7783,0.8152 L0.2933,9.3012 C-0.0977,9.6912 -0.0977,10.3242 0.2933,10.7152 L8.7783,19.2002 Z" />
  </React.Fragment>,
  'Back',
);
